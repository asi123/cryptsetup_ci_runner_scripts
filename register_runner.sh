#!/bin/bash

if [[ ! $# -eq 2 ]]; then
    echo "Usage: $0 <OS name> <Gitlab token>"
    exit 1
fi

OS_NAME="$1"
GITLAB_TOKEN="$2"

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/config.sh

if [ ! -f "$CRYPTSETUP_CI_SCRIPTS_PATH/os/${OS_NAME}.sh" ]; then
    echo "OS $OS_NAME is not supported"
    exit 1
fi

source "$CRYPTSETUP_CI_SCRIPTS_PATH/os/${OS_NAME}.sh"

print_template() {
cat <<EOF
[[runners]]
  executor = "custom"
  builds_dir = "/home/gitlab-runner/builds"
  cache_dir = "/home/gitlab-runner/cache"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.custom]
    prepare_exec = "$RUNNER_INSTALL_PATH/prepare.sh" # Path to a bash script to create VM.
    prepare_args = [ "$OS_NAME" ]
    run_exec = "$RUNNER_INSTALL_PATH/run.sh" # Path to a bash script to run script inside of VM over ssh.
    run_args = [ "$OS_NAME" ]
    cleanup_exec = "$RUNNER_INSTALL_PATH/cleanup.sh" # Path to a bash script to delete VM and disks.
    cleanup_args = [ "$OS_NAME" ]
EOF
}

print_template

gitlab-runner register \
    --template-config <(print_template) \
    --non-interactive \
    --url https://gitlab.com \
    --registration-token $GITLAB_TOKEN \
    --name libvirt-$OS_NAME \
    --tag-list libvirt,$OS_NAME \
    --executor custom
