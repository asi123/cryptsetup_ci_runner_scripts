#!/usr/bin/env bash

export CRYPTSETUP_CI_SCRIPTS_PATH="/home/dzatovic/newscripts"
export PATH="$CRYPTSETUP_CI_SCRIPTS_PATH:$PATH"

VM_ID="runner-$VM_DISTRO-$CUSTOM_ENV_CI_RUNNER_ID-project-$CUSTOM_ENV_CI_PROJECT_ID-concurrent-$CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID-job-$CUSTOM_ENV_CI_JOB_ID"
