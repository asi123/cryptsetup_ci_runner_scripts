#!/bin/sh

if [[ ! $# -eq 1 ]] ; then
    echo "Usage: $0 <VM name>"
    exit 1
fi

VM_NAME="$1"

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/config.sh

source "$CRYPTSETUP_CI_SCRIPTS_PATH/utils_vm.sh"

ssh_vm $(get_vm_ip $VM_NAME)
