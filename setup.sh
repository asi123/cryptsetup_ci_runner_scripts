#!/bin/sh

if [[ ! $# -eq 2 ]] ; then
    echo "Usage: $0 <Gitlab runner token> <number of cuncurrent jobs>"
    exit 1
fi

GITLAB_TOKEN=$1
CONCURRENT_JOBS=$2

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

if [ ! -e ~/.ssh/id_rsa.pub ]; then
   echo "~/.ssh/id_rsa.pub does not exist. Run ssh-keygen first"
   exit 1
fi

# ========== Setup host and runner ==========
dnf -y update

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | os=fedora dist=33 bash

dnf -y install libguestfs-tools-c gitlab-runner
dnf -y group install virtualization
virt-host-validate

systemctl enable --now libvirtd.service

cp -r opt/libvirt-driver/ /opt/

# ===== Create snapshot images =====

# ========== Debian 10 ==========
virt-builder debian-10 \
    --size 8G \
    --output /var/lib/libvirt/images/gitlab-runner-debian10-base.qcow2 \
    --format qcow2 \
    --hostname gitlab-runner-debian10 \
    --network \
    --run-command "apt-get update --allow-releaseinfo-change" \
    --run-command 'useradd -m -p "" gitlab-runner -s /bin/bash' \
    --install git,openssh-server,sudo \
    --ssh-inject gitlab-runner:file:/root/.ssh/id_rsa.pub \
    --run-command "echo 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" \
    --run-command "sed -E 's/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"net.ifnames=0 biosdevname=0\"/' -i /etc/default/grub" \
    --run-command "grub-mkconfig -o /boot/grub/grub.cfg" \
    --run-command "echo 'auto eth0' >> /etc/network/interfaces" \
    --run-command "echo 'allow-hotplug eth0' >> /etc/network/interfaces" \
    --run-command "echo 'iface eth0 inet dhcp' >> /etc/network/interfaces"

# ========== Fedora 34 ==========
virt-builder fedora-34 \
    --size 8G \
    --output /var/lib/libvirt/images/gitlab-runner-fedora34-base.qcow2 \
    --format qcow2 \
    --hostname gitlab-runner-fedora34 \
    --network \
    --run-command 'useradd -m -p "" gitlab-runner -s /bin/bash' \
    --install git,openssh-server,sudo \
    --ssh-inject gitlab-runner:file:/root/.ssh/id_rsa.pub \
    --run-command "echo 'gitlab-runner ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" \
    --selinux-relabel

# ========== Register runners ==========

gitlab-runner register \
    --template-config libvirt.template.fedora34.toml \
    --non-interactive \
    --url https://gitlab.com \
    --registration-token $GITLAB_TOKEN \
    --name ${HOSTNAME}-libvirt-fedora34 \
    --tag-list libvirt,fedora34 \
    --executor custom

gitlab-runner register \
    --template-config libvirt.template.debian10.toml \
    --non-interactive \
    --url https://gitlab.com \
    --registration-token $GITLAB_TOKEN \
    --name ${HOSTNAME}-libvirt-debian10 \
    --tag-list libvirt,debian10 \
    --executor custom

sed -E "s/concurrent = .*/concurrent = $CONCURRENT_JOBS/" -i /etc/gitlab-runner/config.toml

# just for sure, the config should reload automatically anyway
systemctl restart gitlab-runner.service
