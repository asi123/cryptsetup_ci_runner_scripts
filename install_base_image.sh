#!/bin/sh

if [[ ! $# -eq 1 ]] ; then
    echo "Usage: $0 <OS name>"
    exit 1
fi

OS_NAME="$1"

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/config.sh

if [ ! -f "$CRYPTSETUP_CI_SCRIPTS_PATH/os/${OS_NAME}.sh" ]; then
    echo "OS $OS_NAME is not supported"
    exit 1
fi
source "$CRYPTSETUP_CI_SCRIPTS_PATH/os/${OS_NAME}.sh"

DISK_SIZE=${OS_DISK_SIZE:-$DISK_SIZE}
RAM_SIZE=${OS_RAM_SIZE:-$RAM_SIZE}

echo "Installing $OS_NAME to $VM_BASE_IMAGE (disk size $DISK_SIZE, ram size is $RAM_SIZE)"

lvremove -y /dev/$VM_VG/$VM_BASE_LV
lvcreate -V $DISK_SIZE --thin -n $VM_BASE_LV $VM_VG/$THIN_POOL || exit 1

build_os_image $VM_BASE_IMAGE || {
    echo "OS build failed"
    lvremove -y $VM_BASE_IMAGE
}
