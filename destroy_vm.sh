#!/bin/sh

if [[ ! $# -eq 1 ]] ; then
    echo "Usage: $0 <VM name>"
    exit 1
fi

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/config.sh

VM_NAME="$1"
VM_DISK=$VM_IMAGES_PATH/$VM_NAME

ARTIFACTS_NAME=$VM_NAME-artifacts
ARTIFACTS_DISK=$VM_IMAGES_PATH/$ARTIFACTS_NAME

# Destroy VM.
virsh destroy "$VM_NAME"

# Undefine VM.
virsh undefine "$VM_NAME"

echo $ARTIFACTS_DISK
echo $VM_DISK

#kpartx -d /dev/$VM_VG/$ARTIFACTS_NAME

# Delete VM disk.
if [ -b "$ARTIFACTS_DISK" ]; then
    umount -f $ARTIFACTS_DISK
    lvremove -y $VM_VG/$ARTIFACTS_NAME
fi

if [ -b "$VM_DISK" ]; then
    lvremove -y $VM_VG/$VM_NAME
fi

rm -f /tmp/console-$VM_NAME
